# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/24 16:07
# @Author   : Li Jie
# @Site     : http://www.cdtest.cn/
# @File     : unittest测试框架.py
# @Software : PyCharm

"""
什么是测试框架：
1.提供标准测试用例结构
2.提供实际结果与预期结果的验证
3.提供批量化执行测试用例
4.提供测试报告
"""

import unittest  # 导入测试框架

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 导入显式等待类
from selenium.webdriver.support import expected_conditions  # 导入等待条件


class JDtest(unittest.TestCase):  # 必须继承TestCase类
    """
    标准的测试用例分为：测试前，测试中，测试后
    """

    # 测试前：前置条件
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        # 生成显式等待对象，设置最大元素等待时间
        self.wait = WebDriverWait(self.driver, 30)

        self.driver.maximize_window()

        self.driver.get("https://www.jd.com/")


    # 测试中：测试步骤
    def test(self):
        # 加入显式等待
        self.wait.until(expected_conditions.visibility_of_element_located((By.ID, "key")))
        # self.wait.until(expected_conditions.presence_of_element_located((By.ID, "key")))
        self.driver.find_element(By.ID, "key").send_keys("T恤")  # 输入搜索输入框
        self.driver.find_element(By.XPATH, '//*[@id="search"]/div/div[2]/button').click()  # 点击搜索按键

        self.wait.until(
            expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="J_filter"]/div[1]/div[1]/a[2]')))
        self.driver.find_element(By.XPATH, '//*[@id="J_filter"]/div[1]/div[1]/a[2]').click()  # 点击销量

        self.wait.until(expected_conditions.visibility_of_element_located((By.XPATH,'//*[@id="J_goodsList"]/ul/li[2]/div/div[3]/strong/i')))
        #获取综合排名第一产品价格
        price=self.driver.find_element(By.XPATH,'//*[@id="J_goodsList"]/ul/li[2]/div/div[3]/strong/i').text

        #断言：验证实际结果与预期结果是否一致
        self.assertEqual("99.00",price)

    # 测试后：清理测试环境
    def tearDown(self) -> None:
        self.driver.quit()
