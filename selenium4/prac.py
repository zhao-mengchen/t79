# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/24 17:12
# @Author   : Li Jie
# @Site     : http://www.cdtest.cn/
# @File     : prac.py
# @Software : PyCharm

# 按照标准unittest编写一条百度地图的导航测试用例

import unittest  # 导入测试框架

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 导入显式等待类
from selenium.webdriver.support import expected_conditions  # 导入等待条件
from selenium.webdriver.common.action_chains import ActionChains


class BaiduTest(unittest.TestCase):  # 必须继承TestCase类
    """
    标准的测试用例分为：测试前，测试中，测试后
    """

    # 测试前：前置条件
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        # 生成显式等待对象，设置最大元素等待时间
        self.wait = WebDriverWait(self.driver, 30)

        self.driver.maximize_window()

        self.driver.get("https://map.baidu.com/@11585681,3561765,15z")

    # 测试中：测试步骤
    def test(self):
        # 关闭登录窗口
        self.driver.find_element(By.CLASS_NAME, "close-btn").click()  # 关闭登录窗口

        # 输入搜索输入框
        self.wait.until(expected_conditions.visibility_of_element_located((By.ID, "sole-input")))
        self.driver.find_element(By.ID, "sole-input").send_keys("熊猫基地")
        self.driver.find_element(By.ID, "search-button").click()

        # 点击搜索结果的第一项
        self.wait.until(
            expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="card-1"]/div/div[1]/ul/li[1]')))
        self.driver.find_element(By.XPATH, '//*[@id="card-1"]/div/div[1]/ul/li[1]').click()

        # 点击到这去
        self.wait.until(expected_conditions.visibility_of_element_located((By.ID, 'route-go')))
        self.driver.find_element(By.ID, 'route-go').click()

        #输入起点
        self.wait.until(expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="route-searchbox-content"]/div[2]/div/div[2]/div[1]/input')))
        self.driver.find_element(By.XPATH, '//*[@id="route-searchbox-content"]/div[2]/div/div[2]/div[1]/input').clear()
        self.driver.find_element(By.XPATH,
                                 '//*[@id="route-searchbox-content"]/div[2]/div/div[2]/div[1]/input').send_keys(
            "丰德国际广场")


        # 点击驾车
        self.wait.until(expected_conditions.visibility_of_element_located(
            (By.XPATH, '//*[@id="route-searchbox-content"]/div[1]/div[1]/div[2]')))
        self.driver.find_element(By.XPATH, '//*[@id="route-searchbox-content"]/div[1]/div[1]/div[2]').click()
        self.driver.find_element(By.ID, "search-button").click()

        # 关闭百度app登录提示
        time.sleep(1)
        self.wait.until(
            expected_conditions.visibility_of_element_located((By.CLASS_NAME, 'close-btn-download-banner')))
        self.driver.find_element(By.CLASS_NAME, 'close-btn-download-banner').click()

        # 拖拽终点图标
        self.wait.until(
            expected_conditions.visibility_of_element_located((By.XPATH, '//*[@id="platform"]/div[2]/div[2]/div[5]')))
        ActionChains(self.driver).drag_and_drop_by_offset(
            self.driver.find_element(By.XPATH, '//*[@id="platform"]/div[2]/div[2]/div[5]'), -200, 200).perform()
        time.sleep(3)

    # 测试后：清理测试环境
    def tearDown(self) -> None:
        self.driver.quit()
