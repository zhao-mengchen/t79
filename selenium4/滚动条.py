# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/24 10:51
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 滚动条.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from selenium.webdriver.common.by import  By
from  selenium. webdriver.common.action_chains import ActionChains

def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()

    driver.get('https://map.baidu.com/')
    time.sleep(2)
    driver.find_element(By.CLASS_NAME,'close-btn').click()

    #单击鼠标右键
    #contex_click(element=None):
    #没有参数，默认参数值是None，右键点击鼠标当前位置（打开网页后的鼠标当前位置是坐标原点）

    ActionChains(driver).context_click().perform()
    time.sleep(3)

    #模拟鼠标双击地图
    ActionChains(driver).double_click().perform()
    time.sleep(3)
    #鼠标左键按住不放
    ActionChains(driver).click_and_hold().perform()
    time.sleep(3)
    #根据距离移动鼠标
    ActionChains(driver).move_by_offset(500,500).perform()
    time.sleep(4)

    #释放鼠标
    ActionChains(driver).release().perform()
    time.sleep(3)

    #拖拽鼠标
    #参数：element 如果点击当前鼠标位置，传值为None
    #2.xiffset：x轴移动距离
    #3.yossset：y轴移动距离
    ActionChains(driver).drag_and_drop_by_offset(None,-500,-500).perform()
    time.sleep(5)

    #ActionChains 可以将多个操作构建成动作的链接条依次执行
    ActionChains(driver).click_and_hold().move_by_offset(600,600).release().perform()
    time.sleep(3)

    driver.quit()
if __name__=="__main__":
    fun1()