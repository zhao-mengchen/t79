# _*_ coding: UTF-8 _*_
# @Time     : 2022/5/16 10:51
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : App的启动与关闭.py
# @Software : PyCharm
import unittest
from appium import webdriver
import time

class CalculaTest(unittest.TestCase):
    #前置条件
    def setUp(self) -> None:
        # 手机设置
        desired_caps = {}
        desired_caps['platformName'] = 'Android'  # 系统名称
        desired_caps['platformVersion'] = '7.1.2'  # 系统的版本号
        desired_caps['deviceName'] = 'Android Emulator'  # 设备名称，这里是虚拟机，这个没有严格的规定
        desired_caps['appPackage'] = 'com.example.calculator'  # APP包名
        desired_caps['appActivity'] = 'com.example.calculator.MainActivity'  # APP入口的activity
        desired_caps['noReset'] = True  # 不重置app的缓存文件
        desired_caps['unicodeKeyboard'] = True  # 设置键盘支持中文输入
        desired_caps['resetKeyboard'] = True  # 重置键盘

        # 连接appium server，告诉appium，代码要操作哪个设备上的哪个APP
        # 启动手机上App
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)

    #测试步骤
    def test(self):
        time.sleep(5)

    #后置处理
    def tearDown(self) -> None:
        self.driver.quit()
