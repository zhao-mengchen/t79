# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 14:47
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 单选和复选框.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

def fun1():
    """
    单选框radiodbox和checkbox的选择直接点击
    :return:
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"C:\python11\t79\selenium3\example.html")
    #判断元素是否被选中
    iss_elected = driver.find_element(By.ID,'web').is_selected()
    print(iss_elected)

    #勾选复选框

    driver.find_element(By.ID,"web").click()

    iss_elected = driver.find_element(By.ID, 'web').is_selected()
    print(iss_elected)

    time.sleep(1)
    driver.quit()

if __name__=="__main__":
    fun1()