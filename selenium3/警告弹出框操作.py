# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 10:51
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 警告弹出框操作.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By

def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"C:\python11\t79\selenium3\example.html")
    #1.alert 弹出框
    driver.find_element(By.NAME,'alterbutton').click()
    time.sleep(1)
    #切换到弹出框，并获取弹出框对象
    alert = driver.switch_to.alert
    #点击确定
    alert.accept()
    time.sleep(2)

    #2.confirm 弹出框
    driver.find_element(By.NAME,'confirmbutton').click()
    # 切换到弹出框，并获取弹出框对象
    confirm= driver.switch_to.alert
    #点击取消
    confirm.dismiss()
    time.sleep(1)
    #点击确定
    confirm.accept()
    time.sleep(2)
    #3.prompt弹出框
    driver.find_element(By.NAME,'promptbutton') .click()
    prompt  = driver.switch_to.alert

    prompt.send_keys("汇智动力")
    time.sleep(2)
    prompt.accept()
    time.sleep(1)
    prompt.accept()
    time.sleep(1)

    driver.quit()

if __name__=="__main__":
    fun1()