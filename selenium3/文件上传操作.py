# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 10:38
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 文件上传操作.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By

def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"C:\python11\t79\selenium3\example.html")
    #字符串里 r 禁止字符转义
    time.sleep(1)
    #
    el = driver.find_element(By.NAME,"attach[]").send_keys(r'C:\Users\Administrator\Desktop\四川大学\网页\1.gif')
    time.sleep(1)


    driver.quit()

if __name__=="__main__":
    fun1()