# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 9:54
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 下拉菜单操作.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"C:\python11\t79\selenium3\example.html")
    #字符串里 r 禁止字符转义
    time.sleep(1)
    #找到下拉菜单元素<select>
    el = driver.find_element(By.ID,"Selector")
    #构造下拉菜单类的对象
    select = Select(el)

    #根据optin的value属性选择
    select.select_by_value('peach')
    time.sleep(1)
    #根据option的text
    select.select_by_visible_text('3.香蕉')
    time.sleep(1)
    #根据option的索引值
    select.select_by_index(4)
    time.sleep(1)
    driver.quit()
time.sleep(100)
if __name__=="__main__":
    fun1()