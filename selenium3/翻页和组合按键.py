# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 16:57
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 翻页和组合按键.py
# @Software : PyCharm
from selenium import  webdriver
import  time

from selenium.webdriver.common import keys
from  selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"http://news.baidu.com/")

    driver.find_element(By.XPATH,'/html').send_keys(Keys.PAGE_DOWN)

    for i in range(5):
        driver.find_element(By.XPATH, '/html').send_keys(Keys.PAGE_UP)
        time.sleep(1)

    #组合按键
    driver.find_element(By.XPATH, '/html').send_keys(Keys.CONTROL,'a')


    time.sleep(2)
    driver.quit()

if __name__=="__main__":
    fun1()