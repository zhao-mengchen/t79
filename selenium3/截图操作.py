# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 17:21
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 截图操作.py
# @Software : PyCharm
from selenium import  webdriver
import  time

from selenium.webdriver.common import keys
from  selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"http://news.baidu.com/")

    #截取整个屏幕保存为图片
    # driver.get_screenshot_as_file('./截屏.png')

    #截取元素的显示为图片
    driver.find_element(By.XPATH,'//*[@id="sbox"]/tbody/tr/td[1]/div[1]/a/img').screenshot("./元素截图.png")



    time.sleep(2)
    driver.quit()

if __name__=="__main__":
    fun1()