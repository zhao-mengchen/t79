# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 16:14
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 多窗口的切换.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get(r"http://news.baidu.com/")
    #循环打开6个热点
    for i in range(1,7):
        driver.find_element(By.XPATH,f'//*[@id="pane-news"]/div/ul/li[{i}]').click()
        time.sleep(1)
    #获取所有窗口的handle
    handles = driver.window_handles
    print(handles)
    #循环切窗口
    for handle in handles:
        driver.switch_to.window(handle)
        #根据页面url或者title判断页面
        if driver.current_url == "https://wap.peopleapp.com/article/6643479/6518578":
            break
        time.sleep(1)

    time.sleep(2)
    driver.quit()

if __name__=="__main__":
    fun1()