# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/22 15:13
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 内嵌网页的操作.py
# @Software : PyCharm
from selenium import  webdriver
import  time
from  selenium.webdriver.common.by import By


def fun1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://music.163.com/#/discover/toplist?id=3778678")
    #切换到内嵌页面iframe
    #
    # driver.switch_to.frame("g_iframe")

    #点击播放图标
    driver.find_element(By.XPATH,'//span[@data-res-id="1901371647"]').click()



    time.sleep(10)
    driver.quit()

if __name__=="__main__":
    fun1()