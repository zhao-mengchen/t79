# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/21 10:03
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 元素的查找和定位.py
# @Software : PyCharm
"""
元素的查找是基于WebDriver的对象driver
元素的操作基于webElement
"""
from selenium import webdriver
import  time

from selenium.webdriver.common.by import By


def foo2():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    #先通过driver找到元素
    element=driver.find_element(By.ID,"kw")
    #对找到的元素进行操作
    element.send_keys("51放假安排")#模拟键盘输入
    element = driver.find_element(By.ID, "su")
    element.click()
    time.sleep(5)
    driver.quit()
def foo3():
    """
    通过name属性定位
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.NAME,"wd").send_keys("rng")
    driver.find_element(By.ID, "su").click()
    time.sleep(4)
    driver.quit()
def foo4():
    """
    通过classname定位多个元素
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    elements=driver.find_elements(By.CLASS_NAME,"mnav")
    for el in elements:
        print(el.text)
    time.sleep(4)
    driver.quit()
def foo5():
    """
    通过ink text定位多个元素
    只针对链接文本
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.LINK_TEXT,"新闻").click()#模拟鼠标单击
    time.sleep(4)
    driver.quit()
def foo6():
    """
    通过Partial Link text定位元素
    部分链接定位
    只针对链接文本
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.PARTIAL_LINK_TEXT,"hao").click()#模拟鼠标单击
    time.sleep(4)
    driver.quit()
def foo7():
    """
    通过tag name定位元素
    通过标签名定位多个元素
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    els=driver.find_elements(By.TAG_NAME,"a")
    for el in  els:
        print(el.text)
    time.sleep(4)
    driver.quit()
def foo8():
    """
    通过css selector定位元素
    通过css选择器定位元素
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.CSS_SELECTOR,"#s-top-left > a:nth-child(3)").click()
    time.sleep(4)
    driver.quit()
def foo9():
    """
    通过xpath定位元素
    最强大最慢的定位方式
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.XPATH,'//*[@id="s-top-left"]/a[4]').click()
    time.sleep(4)
    driver.quit()
if __name__=="__main__":
    # foo2()
    # foo3()
    # foo4()
    # foo5()
    # foo6()
    # foo7()
    # foo8()
    foo9()