# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/21 15:20
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 元素的基本操纵.py
# @Software : PyCharm
from selenium import webdriver
import  time

from selenium.webdriver.common.by import By
def foo1():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.NAME, "wd").clear()
    driver.find_element(By.NAME,"wd").send_keys("日历")
    driver.find_element(By.ID, "su").click()
    time.sleep(4)
    driver.find_element(By.NAME, "wd").clear()
    driver.find_element(By.NAME,"wd").send_keys("rng")
    driver.find_element(By.ID, "su").click()
    time.sleep(4)
    driver.quit()
def foo2():
    """
    通过name属性定位
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    el= driver.find_element(By.XPATH,'//*[@id="s-top-left"]/a[1]')
    att=el.get_attribute('href')
    #获取元素文本
    text=el.text
    print(text)
    print(att)
    time.sleep(4)
    driver.quit()
if __name__=="__main__":
    # foo1()
    foo2()