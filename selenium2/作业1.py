# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/21 16:03
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : 作业1.py
# @Software : PyCharm
#八种元素定位方法作业练习

from selenium import webdriver
import time
from selenium.webdriver.common.by import By

def foo1():
    """
    通过name属性定位
    :return: None
    """
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.baidu.com/")
    driver.find_element(By.NAME,"f")
    time.sleep(4)
    driver.quit()


if __name__ == "__main__":
    foo1()
