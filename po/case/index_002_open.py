# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/25 15:34
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : index_002_open.py
# @Software : PyCharm
import unittest
import unittest  # 导入测试框架

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 导入显式等待类
from selenium.webdriver.support import expected_conditions  # 导入等待条件
from selenium.webdriver.common.action_chains import ActionChains
from po.page.index_page import IndexPage

class Index001(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.wait = WebDriverWait(self.driver, 30)
        # 生成首页对象
        self.index_page = IndexPage(self.driver,self.wait)
    def test(self):
        self.driver.get("https://www.baidu.com/")
        self.driver.maximize_window()
        self.index_page.move_to_settings()

        time.sleep(5)


    def tearDown(self) -> None:
        self.driver.quit()