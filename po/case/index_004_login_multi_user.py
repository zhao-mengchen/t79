# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/25 15:55
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : index_003_login.py
# @Software : PyCharm
import unittest
import unittest  # 导入测试框架

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 导入显式等待类
from selenium.webdriver.support import expected_conditions  # 导入等待条件
from selenium.webdriver.common.action_chains import ActionChains
from po.page.index_page import IndexPage#导入首页类
from po.common.read_write import read_csv
class Index004(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.wait = WebDriverWait(self.driver, 30)

        self.index_page = IndexPage(self.driver,self.wait)

        self.data = read_csv(r'C:\python11\t79\po\data\users.csv')
    def test(self):
        for users in self.data:
            self.driver.get("https://www.baidu.com/")
            self.driver.maximize_window()

            self.index_page.click_login_button()
            self.index_page.input_user(users[0])
            self.index_page.input_pwd(users[1])
            self.index_page.input_login()


        # time.sleep(5)


    def tearDown(self) -> None:
        self.driver.quit()