# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/25 14:32
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : index_page.py
# @Software : PyCharm
import unittest  # 导入测试框架

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait  # 导入显式等待类
from selenium.webdriver.support import expected_conditions  # 导入等待条件
from selenium.webdriver.common.action_chains import ActionChains

class IndexPage():
    """
    定义百度首页
    """
    #定义属性
    def __init__(self,driver,wait):
        #获取从用例层传入driver和wait
        self.driver = driver
        self.wait = wait
        #调试使用
        # self.driver = webdriver.Chrome()
        # self.wait = WebDriverWait(self.driver, 30)
        #
        self.search_box_locator = (By.ID,"kw")
        self.search_button_locator = (By.ID,"su")
        self.setting_btn_locator =(By.ID,"s-usersetting-top")
        self.login_btn_locator = (By.ID,"s-top-loginbtn")
        self.user_input_locator =(By.ID,"TANGRAM__PSP_11__userName")
        self.pwd_input_locator =(By.ID,"TANGRAM__PSP_11__password")
        self.login_btn_locator2 =(By.ID,"TANGRAM__PSP_11__submit")
    #定义方法
    def input_search_box(self, text):
        """
        输入搜索输入框
        :return:
        """
        self.wait.until(expected_conditions.
        visibility_of_element_located((self.search_box_locator)))
        el = self.driver.find_element(*self.search_box_locator)
        el.clear()
        el.send_keys(text)
    def click_search_button(self):
        """
        点击搜索按钮
        :return:

        """
        self.wait.until(expected_conditions.
        visibility_of_element_located((self.search_button_locator)))
        el=self.driver.find_element(*self.search_button_locator)
        el.click()

    def move_to_settings(self):
        """
        移动鼠标到设置
        :return:
        """
        self.wait.until(expected_conditions.visibility_of_element_located((self.setting_btn_locator)))
        el=self.driver.find_element(*self.setting_btn_locator)
        ActionChains(self.driver).move_to_element(el).perform()

    def click_login_button(self):
        """

        :return:
        """
        self.wait.until(expected_conditions.visibility_of_element_located((self.login_btn_locator)))
        el = self.driver.find_element(*self.login_btn_locator)
        el.click()


    def input_user(self,useranme):
        """
        输入账号
        :return:
        """
        self.wait.until(expected_conditions.visibility_of_element_located((self.user_input_locator)))
        el = self.driver.find_element(*self.user_input_locator)
        el.send_keys(useranme)
    def input_pwd(self,password):
        """
        输入密码
        :return:
        """
        self.wait.until(expected_conditions.visibility_of_element_located((self.pwd_input_locator)))
        el = self.driver.find_element(*self.pwd_input_locator)
        el.send_keys(password)

    def input_login(self):
        """
        点击登录
        :return:
        """
        self.wait.until(expected_conditions.visibility_of_element_located((self.login_btn_locator2)))
        el = self.driver.find_element(*self.login_btn_locator2)
        el.click