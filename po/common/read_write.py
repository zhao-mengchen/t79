# _*_ coding: UTF-8 _*_
# @Time     : 2022/4/25 16:07
# @Author   : user
# @Site     : http://www.cdtest.cn/
# @File     : read_write.py
# @Software : PyCharm
import csv

def read_csv(filepath):
    with open(filepath,mode="r",encoding='utf8')as  file:
        data = []
        data = csv.reader(file)
        data = list(data)
    return data

if __name__ =="__main__":
    print(read_csv('../data/users.csv'))