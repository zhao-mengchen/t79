# coding:UTF-8
# ___________________________
# Project_Name:t79
# File_Name:llq基本操作.py
# Author:Administrator
# Date_time:2022/4/20
# Description:
# ___________________________
from selenium import  webdriver
import time

def foo1():
    driver = webdriver.Chrome()
    time.sleep(2)
    #最大化浏览器
    driver.maximize_window()
    #浏览器打开指定url
    driver.get("https://www.1ppt.com/")
    driver.back()#浏览器后退
    time.sleep(2)
    #浏览器前进
    driver.forward()
    time.sleep(2)
    #浏览器刷新
    driver.refresh()
    time.sleep(2)
    #获取网页标题
    title= driver.title
    #获取网页url
    url= driver.current_url
    driver.close()#关闭浏览器窗口
    print(f'网页标题：{title},网页地址：{url}')
    # driver.quit()#关闭浏览器
if __name__=="__main__":
    foo1()
